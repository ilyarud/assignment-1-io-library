section .text

%define SPACE 0x20
%define TAB 0x9
%define NEWLINE 0xA

%define READ 0
%define WRITE 1
%define IN 0
%define OUT 1
%define EXIT 60

; Принимает код возврата и завершает текущий процесс
exit: 
    mov  rax, EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi+rax], 0
        je .break
        inc rax
        jmp .loop
    .break:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov rdx, rax,
    pop rsi
    mov rax, WRITE
    mov rdi, OUT
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp   
    mov rax, WRITE 
    mov rdx, 1     
    mov rdi, OUT   
    syscall

    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE
    jmp print_char


; Выводит беззнаковое 8-байтовое число в десятичном формате 
print_uint:
    xor rcx, rcx              ; counter; to align rsp later  

    mov rax, rdi              ; rdi -> rax
    xor rdx, rdx              ; clear rdx
    mov r10, 10               ; divider 
    dec rsp                
    .loop:
        div r10                   ; rax / 10, reminder -> rdx
        add dl, '0'               ; reminder to ASCII
        dec rsp
        inc rcx
        mov byte [rsp], dl        ; save
        xor rdx, rdx              ; clear
        cmp rax, 0                ; check if result of division != 0
        jnz .loop
    
    mov rdi, rsp              ; beginning of number
    push rcx
    call print_string
    pop rcx
    inc rcx
    
    add rsp, rcx               ; restore rsp

    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0                  ; if number >= 0: print uint
    jge .uint   
    neg rdi                     ; invert number
    push rdi                    ; save 
    mov rdi, '-'                ; print '-'
    call print_char 
    pop rdi                     ; print number as uint
    .uint:
        jmp print_uint
    

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .loop:
        mov r9b, byte [rsi]     ; current byte of 1st string -> r9
        mov r10b, byte [rdi]    ; current byte of 2nd string -> r10

        cmp r9b, r10b             ; well... quite obvious what happens here
        jne .not_equal
        test r9b, r10b            ; if we at the end 
        jz .equal

        inc rsi                 ; check next byte
        inc rdi

        jmp .loop

    .equal:
        mov rax, 1
        ret
    .not_equal:
        xor rax, rax            ; 0 -> rax
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, READ               ; 'read' syscall
    mov rdi, IN                 ; stdin
    mov rdx, 1                  ; length (1 char)

    push 0                      ; reserve for input
    mov rsi, rsp                
    syscall

    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; rdi - буфер, rsi - размер

read_word:
    push r12
    push r13
    push r14
    mov r12, rdi            ; buffer
    mov r13, rsi            ; size
    dec r13                 ; extra space for null-terminator
    xor r14, r14            ; current length
    
    .loop:
        call read_char
        cmp rax, 0       ; if EOF 
        jle .break

        cmp rax, SPACE      ; comparing and skipping if eq. SPACE, TAB or NEWLINE
        je .continue    
        cmp rax, TAB
        je .continue
        cmp rax, NEWLINE
        je .continue

        cmp r14, r13        ; if length >= buffer: error
        jge .error

        mov [r12+r14], rax  ; save to buffer
        inc r14
        jmp .loop
    .continue:
        test r14, r14      
        jz .loop
    .break:
        mov byte[r12+r14+1], 0
        jmp .end
    .error:
        xor r12, r12
    .end:
        mov rax, r12
        mov rdx, r14
    
    pop r14
    pop r13
    pop r12
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push rbx   
    
    xor rdx, rdx                    ; clearing registers
    xor rax, rax
    xor rbx, rbx 

    .loop:
        mov bl, byte [rdi + rdx]    ; current char
        sub bl, '0'                 ; ascii to number
        jl .break                   ; if !number: break
        cmp bl, 9   
        jg .break         
        imul rax, rax, 10           ; rax * 10 -> rax    
        add rax, rbx                ; result += rbx 
        inc rdx                     ; next char
        jmp .loop
    .break:    
        pop rbx
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'        ; if starts with '-'
    je .neg               
    jmp parse_uint
	.neg:
		inc rdi                
		call parse_uint        
		cmp rdx, 0             ; check if parsed successfuly
		je .error               
		inc rdx                ; +1 for '-' sign
		neg rax                
		ret
	.error:
		xor rax, rax            ; 0 -> rax
		ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rdi - src
; rsi - dest
; rdx - buffer length
string_copy:
    xor rax, rax                    ; counter (length)

    .loop:
        cmp rax, rdx                ; if length >= buffer size: error
        jge .error
        mov r8b, byte [rdi + rax]   ; src string char
        mov byte [rsi + rax], r8b   ; write char to dest
        test r8b, r8b               ; null terminator check
        jz .break
        inc rax
        jmp .loop
    .error:
        xor rax, rax
    .break:
        ret
